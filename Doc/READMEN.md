**Installation**


```shell
<VirtualHost *:80>
        ServerName caja.app.local
        SetEnv APP_ENVIRONMENT dev
        DocumentRoot /var/www/html/caja/public/www/
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

