<?php

use Exception;
use Throwable;

class CoreException extends Exception
{
    /**
     * Data relevant to the exception
     *
     * @var array
     */
    protected array $data = [];

    /**
     * CoreException constructor.
     *
     * @param string $message
     * @param array $data
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = '', array $data = [], $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->data = $data;
    }

    /**
     * Returns the data relevant to the exception
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Sets the data relevant to the exception
     *
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}