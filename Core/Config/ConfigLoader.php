<?php




class ConfigLoader
{
    /**
     * @param array $files
     * @return mixed
     * @throws ConfigException
     */
    public static function load(array $files)
    {
        $content = '';
        foreach ($files as $file) {
            if (!is_file($file)) {
                throw new ConfigException("The configuration file " . $file . " doesn't exist");
            }
            $content .= PHP_EOL . file_get_contents($file);
        }

        $configuration = Yaml::parse($content);
        array_walk_recursive(
            $configuration,
            function (&$value) {
                $matches = [];
                if (preg_match('/%env\((.+)\)%/', $value, $matches)) {
                    $value = getenv($matches[1]);
                }
            }
        );

        return $configuration;
    }

}