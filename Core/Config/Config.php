<?php

class Config
{
    /**
     * @var array
     */
    protected static array $config = [];

    /**
     * Init Config
     * @param array $value
     * @throws ConfigException
     */
    public static function init(array $value)
    {
        if (!is_array($value)) {
            throw new ConfigException('Init must use an array');
        }

        static::$config = $value;
    }

    /**
     * Push a new setting to an existing property.
     *
     * @param string $path
     * @param $value
     * @throws ConfigException
     */
    public static function push(string $path, $value)
    {
        static::get($path);
        static::set($path . '.' . $value, $value);
    }

    /**
     * Returns the configured values for the specified configuration path
     *
     * @param string $path
     * @param null $default
     * @return array|mixed
     * @throws ConfigException
     */
    public static function get(string $path = '', $default = null)
    {
        if (empty($path)) {
            return static::$config;
        }

        $parts = explode('.', $path);
        $ret = static::$config;

        foreach ($parts as $part) {
            if (!isset($ret[$part])) {
                if (!isset($default)) {
                    throw new ConfigException($path . ' is not accessible in config and no default was specified');
                }

                return $default;
            }

            $ret = $ret[$part];
        }

        return $ret;
    }

    /**
     * Set the specified configuration value for the specified path
     *
     * @param string $path
     * @param $value
     * @throws ConfigException
     */
    public static function set(string $path, $value)
    {
        //Build Structure
        $output = null;
        $temp =& $output;

        foreach (explode('.', $path) as $part) {
            if (!isset($part)) {
                throw new ConfigException($path . ' cannot contain empty nodes');
            }

            $temp[$part] = [];
            $temp =& $temp[$part];
        }

        $temp = $value;

        //merge it with the current configuration
        static::$config = array_replace_recursive(static::$config, $output);
    }

    /**
     * @param string $path
     * @return bool
     */
    public static function check(string $path = ''): bool
    {
        if (empty($path)) {
            return false;
        }

        $parts = explode('.', $path);
        $ret = static::$config;

        foreach ($parts as $part) {
            if (!isset($ret[$part])) {
                return false;
            }

            $ret = $ret[$part];
        }

        return true;
    }

}