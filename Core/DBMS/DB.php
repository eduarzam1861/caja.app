<?php


use Config;
use ConfigException;
use CoreException;
use DBException;
use mysqli;
use mysqli_result;
use mysqli_sql_exception;
use OutOfBoundsException;

class DB
{
    /**
     * @var DB
     */
    protected static ?DB $instance = null;

    /**
     * @var array $connections
     */
    protected array $connections = [];

    /**
     * Contains a list of all executed queries
     *
     * @var array $queriesExecuted
     */
    protected static array $queriesExecuted = [];

    /**
     * The amount of executed queries
     *
     * @var int $queryCount
     */
    protected static int $queryCount = 0;


    /**
     * DB constructor.
     * @throws ConfigException
     */
    protected function __construct()
    {
        $this->connections = [];
        if (Config::check('mysql.connections')) {
            $this->connections = Config::get('mysql.connections');
        }
    }

    /**
     * Singleton
     */
    public static function getInstance(): ?DB
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        mysqli_report(MYSQLI_REPORT_STRICT);

        return static::$instance;
    }


    /**
     * Expose the connection socket
     *
     * @param string $connection
     * @return mixed
     */
    public static function getConnection(string $connection = 'default')
    {
        $instance = static::getInstance();
        return $instance->connect($connection);
    }

    /**
     * @param string $connection
     * @param array $config
     */
    public static function setConnection(string $connection, array $config): void
    {
        $instance = static::getInstance();
        $instance->connections[$connection] = $config;
    }

    /**
     * Maintain Connection
     *
     * @param string $connection
     * @param bool $with_db
     * @return mixed
     * @throws DBException
     */
    private function connect(string $connection, bool $with_db = true)
    {
        if (!isset($this->connections[$connection])) {
            throw new DBException('Connection "' . $connection . '" not available in configuration');
        }

        if (!isset($this->connections[$connection]['socket'])) {
            $config = $this->connections[$connection];

            try {
                $this->connections[$connection]['socket'] = new mysqli(
                    isset_get($config['host']),
                    isset_get($config['user']),
                    isset_get($config['password']),
                    $with_db ? $config['database'] : null,
                    isset_get($config['port'])
                );

                mysqli_set_charset($this->connections[$connection]['socket'], $config['charset']);
            } catch (mysqli_sql_exception $e) {
                throw new DBException($e->getMessage(), [], $e->getCode(), $e);
            }
        }

        return $this->connections[$connection]['socket'];
    }


    /**
     * Get number of rows affected by previous query
     *
     * @param string $connection
     * @return mixed
     * @throws DBException
     */
    public static function affectedRows(string $connection = 'default')
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection);
        try {
            return $socket->affected_rows;
        } catch (mysqli_sql_exception $e) {
            throw new DBException($e->getMessage(), [], $e->getCode(), $e);
        }
    }

    /**
     * Get number of fields in the result
     *
     * @param string $connection
     * @return mixed
     * @throws DBException
     */
    public static function numFields(string $connection = 'default')
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection);

        try {
            return $socket->field_count;
        } catch (mysqli_sql_exception $e) {
            throw new DBException($e->getMessage(), [], $e->getCode(), $e);
        }
    }

    /**
     * @param string $connection
     * @return mixed
     * @throws DBException
     */
    public static function insertId(string $connection = 'default')
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection);

        return $socket->insert_id;
    }

    /**
     * Returns if specified index exists
     *
     * @param string $table
     * @param string $index
     * @param string $connection
     * @return array|bool|null
     * @throws DBException
     */
    public static function indexExists(string $table, string $index, string $connection = 'default')
    {
        if (static::tableExists($table)) {
            $query = "SHOW INDEX FROM " . cfm($table) . " WHERE `Key_name` = " . cfs($index) . ";";
            $exist = static::query($query, $connection);

            if (!static::numRows($exist) > 0) {
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Return if table exists
     *
     * @param string $table
     * @param string $connection
     * @return bool
     * @throws DBException
     */
    public static function tableExists(string $table, string $connection = 'default'): bool
    {
        $res = static::query("SHOW TABLES LIKE " . cfs($table) . ";", $connection);

        if (static::numRows($res) > 0) {
            return true;
        }

        return false;
    }


    /**
     * Do Simple query
     *
     * @param string $query
     * @param string $connection
     * @return bool|mysqli_result
     * @throws DBException
     */
    public static function query(string $query, string $connection = 'default')
    {
        self::$queryCount++;
        self::$queriesExecuted[] = '[' . $connection . '] ' . $query;

        $instance = static::getInstance();
        $socket = $instance->connect($connection);

        try {
            //Make sure the CREATE TABLE always has the right charset
            if (strstr(strtoupper($query), 'CREATE TABLE')) {
                if (strstr(strtoupper($query), 'CHARSET')) {
                    throw new DBException('Do not add charset to create table queries');
                }

                $query = rtrim(trim($query), ";");
                $config = $instance->connections[$connection];
                $query .= " CHARSET=" . $config['charset'] . " COLLATE=" . $config['collate'] . ";";
            }

            $r = mysqli_query($socket, $query);

            if (!empty(mysqli_errno($socket))) {
                throw new DBException(mysqli_error($socket), ['query' => $query], mysqli_errno($socket));
            }

            return $r;
        } catch (mysqli_sql_exception $e) {
            throw new DBException($e->getMessage(), ['query' => $query], $e->getCode(), $e);
        }
    }


    /**
     * Execute a query if a specific column exists in a table.
     *
     * @param string $table
     * @param string $column
     * @param string $query
     * @param string $connection
     * @throws DBException
     */
    public static function queryIfColumnExists(string $table, string $column, string $query, string $connection = 'default')
    {
        if (!static::tableExists($table)) {
            throw new DBException('Table not found: ' . $table);
        }

        if (static::columnExists($table, $column, $connection)) {
            static::query($query, $connection);
        }
    }

    /**
     * Execute a query if a specific column doesn't exist in a table.
     *
     * @param string $table
     * @param string $column
     * @param string $query
     * @param string $connection
     * @throws DBException
     */
    public static function queryIfColumnDoesNotExist(string $table, string $column, string $query, string $connection = 'default')
    {
        if (!static::tableExists($table)) {
            throw new DBException('Table not found: ' . $table);
        }

        if (!static::columnExists($table, $column, $connection)) {
            static::query($query, $connection);
        }
    }

    /**
     * Execute a query if a specific index doesn't exist in a table.
     *
     * @param string $table
     * @param string $index
     * @param string $query
     * @param string $connection
     * @throws DBException
     */
    public static function queryIfIndexDoesNotExist(string $table, string $index, string $query, string $connection = 'default')
    {
        if (!static::tableExists($table)) {
            throw new DBException('Table not found: ' . $table);
        }

        if (!static::indexExists($table, $index, $connection)) {
            static::query($query, $connection);
        }
    }

    /**
     * Get number of rows in result
     *
     * @param mysqli_result $result
     * @return int
     * @throws DBException
     */
    public static function numRows(mysqli_result $result): int
    {
        try {
            return $result->num_rows;
        } catch (mysqli_sql_exception $e) {
            throw new DBException($e->getMessage(), [], $e->getCode(), $e);
        }
    }

    /**
     * Execute query and return only the first row or column
     *
     * @param string $query
     * @param string|null $column
     * @param bool $strict
     * @param string $connection
     * @return array|null
     * @throws DBException
     */
    public static function get(
        string $query,
        string $column = null,
        bool $strict = false,
        string $connection = 'default'
    ) {
        $result = static::query($query, $connection);
        $rows = static::numRows($result);

        if ($rows == 0) {
            //No rows, return nada
            return null;
        }

        if ($rows > 1) {
            //To many results matched, query needs to be adjusted
            throw new DBException('DB:get expects one result', ['query' => $query]);
        }

        $data = $result->fetch_assoc();

        if ($column == null) {
            //No column data
            return $data;
        }

        if (isset($data[$column])) {
            //Return data from the selected column
            return $data[$column];
        }

        if ($strict == true) {
            //Check if we can't find the column
            throw new DBException('DB:get column not in result data', ['query' => $query]);
        }

        return null;
    }


    /**
     * Same as DB::get() but returns only one column.
     *
     * It is not required to indicate what column must be returned, but it is required that only one value from one
     * row / column will be returned by the query, or an exception will be thrown
     *
     * @param string $query
     * @return mixed
     * @throws DBException
     */
    public static function getColumn(string $query)
    {
        // Get query results
        $results = static::get($query);

        if ($results === null) {
            return null;
        }

        // The query results should have only one column. If more than one column is present, fail
        if (count($results) != 1) {
            throw new DBException(
                'Results for select query "' . Strings::log($query) . '" should contain exactly one column but contain "' . count($results) . '" columns',
                ['query' => $query, 'results' => $results]
            );
        }

        // Return the value from the column
        return array_pop($results);
    }

    /**
     * Drop a column from a table.
     *
     * @param string $table
     * @param string $column
     * @param string $connection
     * @throws DBException
     */
    public static function dropColumn(string $table, string $column, string $connection = 'default')
    {
        if (!static::tableExists($table)) {
            throw new DBException('Table not found: ' . $table);
        }
        if (!static::columnExists($table, $column)) {
            throw new DBException('Column not found on table: ' . $table);
        }

        static::query("ALTER TABLE " . cfm($table) . " DROP COLUMN " . cfm($column) . ";", $connection);
    }

    /**
     * Returns if specified column exists
     *
     * @param string $table
     * @param string $column
     * @param string $connection
     * @return array|bool|null
     * @throws DBException
     */
    public static function columnExists(string $table, string $column, string $connection = 'default')
    {
        if (!static::tableExists($table, $connection)) {
            return false;
        }

        $var = static::get(
            "SHOW COLUMNS FROM " . cfm($table) . " WHERE `Field` = " . cfs($column) . ";",
            '',
            false,
            $connection
        );

        if (empty($var)) {
            return false;
        }
        return true;
    }

    /**
     * Safely rename a table.
     *
     * @param string $from_table
     * @param string $to_table
     * @param string $connection
     * @throws DBException
     */
    public static function renameTable(string $from_table, string $to_table, string $connection = 'default')
    {
        if (!static::tableExists($from_table, $connection)) {
            throw new DBException('Source table does not exist ' . $from_table);
        }

        if (static::tableExists($to_table, $connection)) {
            throw new DBException('Target table already exists ' . $to_table);
        }

        static::query("RENAME TABLE " . cfm($from_table) . " TO " . cfm($to_table) . ';', $connection);
    }

    /**
     * Format timestamp to sql format.
     *
     * @param $timestamp
     * @return bool|string
     */
    public static function dateFormat(int $timestamp)
    {
        return date('Y-m-d H:i:s', $timestamp);
    }

    /**
     * Create a database if is does not exist already
     *
     * @param string $database
     * @param string $connection
     * @return bool
     * @throws DBException
     */
    public static function createDatabase(string $database, string $connection = 'default'): bool
    {
        if (!static::databaseExists($database, $connection)) {
            static::query("CREATE DATABASE " . cfm($database) . ";", $connection);
            static::query("USE " . cfm($database) . ";", $connection);
        }

        return true;
    }

    /**
     * return if database exists
     *
     * @param string $database
     * @param string $connection
     * @return bool
     * @throws DBException
     */
    public static function databaseExists(string $database, string $connection = 'default'): bool
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection, false);
        $res = mysqli_query($socket, "SHOW DATABASES LIKE " . cfs($database) . ";");

        if (static::numRows($res) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Drop a table
     *
     * @param string $table
     * @param string $connection
     * @throws DBException
     */
    public static function dropTable(string $table, string $connection = 'default')
    {
        if (static::tableExists($table, $connection)) {
            static::query("DROP TABLE " . cfm($table), $connection);
        }
    }

    /**
     * Return array with all field in a table.
     *
     * @param string $table
     * @param string $connection
     * @return array
     * @throws DBException
     */
    public static function tableFields(string $table, string $connection = 'default'): array
    {
        if (!static::tableExists($table, $connection)) {
            throw new DBException('Table not found: ' . $table);
        }
        $fields = [];
        $res = static::query("DESCRIBE " . cfm($table), $connection);
        foreach ($res as $field) {
            $fields[$field['Field']] = $field['Type'];
        }
        return $fields;
    }

    /**
     * Delete data from a table and back it up first
     *
     * @param string $table
     * @param string $backup_path
     * @param string $connection
     * @throws DBException
     */
    public static function backupDelete(string $table, string $backup_path, string $connection = 'default')
    {
        if (!static::tableExists($table, $connection)) {
            throw new DBException('Table not found: ' . $table);
        }

        static::backup($table, $backup_path, $connection);
        static::dropDatabase($table, $connection);
    }

    /**
     * Back up part of a table
     *
     * @param string $table
     * @param string $backup_path
     * @param string $connection
     * @throws DBException|ConfigException
     */
    public static function backup(string $table, string $backup_path, string $connection = 'default')
    {
        if (!static::tableExists($table, $connection)) {
            throw new DBException('Table not found: ' . $table);
        }

        if (!file_exists($backup_path)) {
            throw new DBException('Backup path not available: ' . $backup_path);
        }

        $config = Config::get('mysql.connections.' . $connection);

        exec('mysqldump \
        -q \
        --host=' . escapeshellarg($config['host']) . ' \
        --user=' . escapeshellarg($config['user']) . ' \
        --password=' . escapeshellarg($config['password']) . ' \
        --databases ' . escapeshellarg($config['database']) . ' \
        --tables ' . escapeshellarg($table) . ' \
        >' . escapeshellarg($backup_path . '/' . $table) . '.sql \
        2>/dev/null', $output, $exit_code);

        if ($exit_code != 0) {
            throw new DBException('Backup failed with exit code ' . $exit_code);
        }
    }

    /**
     * Drop a Database if it exists
     *
     * @param string $database
     * @param string $connection
     * @return bool
     * @throws DBException
     */
    public static function dropDatabase(string $database, string $connection = 'default'): bool
    {
        if (static::databaseExists($database, $connection)) {
            static::query("DROP DATABASE " . cfm($database) . ";", $connection);
        }
        return true;
    }

    /**
     * Insert data into a table
     *
     * @param string $table
     * @param array $fields
     * @param array $values
     * @param array $updates
     * @param string $connection
     * @return mixed
     * @throws DBException
     */
    public static function insert(
        string $table,
        array $fields,
        array $values,
        array $updates = [],
        string $connection = 'default'
    ) {
        $query = "INSERT INTO " . cfm($table) . " (" . implode(',', $fields) . ") 
            VALUES (" . implode(',', $values) . ")";
        if (!empty($updates)) {
            $query .= " ON DUPLICATE KEY UPDATE " . implode(',', $updates) . ";";
        }

        return static::query($query, $connection);
    }


    /**
     * Start a SQL transaction.
     *
     * @param string $connection
     * @throws DBException
     */
    public static function startTransaction(string $connection = 'default')
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection, false);
        mysqli_begin_transaction($socket);
    }


    /**
     * Commit a SQL transaction
     *
     * @param string $connection
     * @throws DBException
     */
    public static function commitTransaction(string $connection = 'default')
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection, false);
        mysqli_commit($socket);
    }


    /**
     * Rollback a SQL transaction
     *
     * @param string $connection
     * @throws DBException
     */
    public static function rollbackTransaction(string $connection = 'default')
    {
        $instance = static::getInstance();
        $socket = $instance->connect($connection, false);
        mysqli_rollback($socket);
    }


    /**
     * Update single field in table with unique id
     *
     * @param string $table
     * @param int $id
     * @param string $field
     * @param $value
     * @throws DBException
     */
    public static function updateById(string $table, int $id, string $field, $value)
    {

        if (is_string($value)) {
            $value = "'" . cfm($value) . "'";
        }

        self::query("
            UPDATE
                " . cfm($table) . "
            SET
                " . cfm($field) . "=" . $value . "
            WHERE
                id=" . cfi($id) . ";");
    }


    /**
     * Select by field
     *
     * @param string $table
     * @param int $id
     * @param string $field
     * @return mixed
     * @throws DBException
     */
    public static function selectByIntField(string $table, int $id, string $field = 'id')
    {
        return self::get("
            SELECT 
                *
            FROM    
                " . cfm($table) . "
            WHERE
                " . cfm($field) . "=" . cfi($id) . "
            LIMIT 0,1;");
    }


    /**
     * Select by field
     *
     * @param string $table
     * @param string $string
     * @param string $field
     * @return mixed
     * @throws DBException
     */
    public static function selectByStringField(string $table, string $string, string $field = 'id')
    {
        return self::get("
            SELECT 
                *
            FROM    
                " . cfm($table) . "
            WHERE
                " . cfm($field) . "='" . cfm($string) . "'
            LIMIT 0,1;");
    }


    /**
     * Get all results of query
     *
     * @param string $query
     * @param string $connection
     * @return array
     * @throws CoreException
     * @throws DBException
     */
    public static function list(string $query, string $connection = 'default'): array
    {
        $return = [];
        $rows = DB::query($query, $connection);

        foreach ($rows as $row) {
            $return[] = $row;
        }

        return $return;
    }


    /**
     * Returns a list of all executed queries
     *
     * @return array
     */
    public static function getQueriesExecuted(): array
    {
        return self::$queriesExecuted;
    }


    /**
     * Returns the amount of executed queries
     *
     * @return int
     */
    public static function getQueryCount(): int
    {
        return self::$queryCount;
    }


    /**
     * Returns string "NULL" if specified $value is NULL, $value as is if it is numeric, or $value with quotes if it is
     *      not NULL or numeric.
     *
     * @param $value
     * @param string $quote
     * @param bool $force
     * @return mixed
     */
    public static function quote($value, string $quote = '"', bool $force = false): string
    {
        // NULL values get returned as unquoted NULL because that is what MySQL expects for a NULL value.
        if ($value === null) {
            return 'NULL';
        }

        // Numbers don't need quotes
        if (!$force) {
            if (is_numeric($value)) {
                return $value;
            }
        }

        // Booleans will be stored as 1 or 0
        if (is_bool($value)) {
            return $value ? 1 : 0;
        }

        // If this is a string, it should be quoted
        if (is_scalar($value)) {
            return $quote . cfm($value) . $quote;
        }

        // MySQL queries only accept scalar values
        throw new OutOfBoundsException(
            'Specified query value "' . Strings::log($value) . '" is not scalar',
            ['value' => $value]
        );
    }

}
