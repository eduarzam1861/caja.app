FROM centos:7

ARG gitlab_config

# initial package update
RUN yum -y update && yum -y upgrade

# web server installation and web server initialization
RUN yum -y install httpd; yum clean all; systemctl enable httpd.service

# installation of php and its extensions
RUN yum -y install wget
RUN yum update -y \
    && yum install -y epel-release yum-utils \
    && yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
    && yum-config-manager --enable remi-php74 \
    && yum install -y gd gd-devel php php-common php-opcache php-mcrypt php-cli php-gd php-curl \
    && yum install -y php-zip php74-php-imap php-mysqli php-dom php-memcached php-curl php74-php-xml php-xml \
    && yum install -y php74-php-zip php74-php-mbstring php-mbstring php-pecl-redis

# utility installation
RUN yum -y install git openssh-server vim curl neofetch mc

# composer installation
RUN cd /tmp/ && \
    curl -s https://getcomposer.org/installer | php && \
    cp /tmp/composer.phar /usr/bin/composer

# gitlab configuration and server stamp aggregation
RUN echo "$gitlab_config" >> ~/.bashrc && source ~/.bashrc
RUN echo "neofetch" >> ~/.bashrc && source ~/.bashrc

# open port 80
EXPOSE 80
CMD ["/usr/sbin/init"]
# container build command
# > $ sudo docker build -t caja --build-arg gitlab_config="$(cat $(pwd)/Utils/config_gitlab.txt)" .
# command to run the container
# the port can be changed example 5000:80
# please change the ip of run command as your computer may have another ip
# > $ sudo docker run -p 3000:80 --add-host=database:192.168.1.124 -v$(pwd)/parser:/var/www/html -it caja bash