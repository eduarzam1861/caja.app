<?php

try {
    Config::set('mysql', [
        'connections' => [
            'default' => [
                'host' => '127.0.0.1',
                'port' => '3306',
                'user' => 'root',
                'password' => 'hola1808',
                'database' => 'caja_db',
                'charset' => 'utf8mb4',
                'collate' => 'utf8mb4_unicode_ci',
            ],
        ]
    ]);
} catch (ConfigException $e) {
    error_log('config library not accessible');
}